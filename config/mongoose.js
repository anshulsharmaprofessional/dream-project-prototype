//Configuration file of Mongoose to create and connect the Database(MongoDB).

// importing mongoose
const mongoose = require("mongoose");

// connecting mongo db database
const local = "mongodb://localhost/booking-api-guruji-astro";
// || process.env.MONGODB_URI

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  family: 4,
});
const db = mongoose.connection;

// error handling
db.on("error", console.error.bind(console, "Error connecting to MongoDB"));

// conformation message
db.once("open", function () {
  console.log("Connected to Database :: MongoDB");
});

// exporting db  for index.js
module.exports = db;
