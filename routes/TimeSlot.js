// import express
const express = require("express");
// calling router
const router = express.Router();
// importing timeSlot controller
const timeSloteController = require("../controllers/timeSloteController");

//to add a timeslot
router.post("/create", timeSloteController.create);
router.get("/getAllTimeSlot", timeSloteController.getTimeSlot);
router.get("/getTimeSlotWithBooked", timeSloteController.getTimeSlotWithBooked);
router.get("/getTimeSlotByDate", timeSloteController.getTimeSlotByDate);

//exporting the router
module.exports = router;
