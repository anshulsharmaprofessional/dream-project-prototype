// importing express instance
const express = require("express");
// getting express router
const router = express.Router();

//Further routes
router.use("/booking", require("./Booking"));
router.use("/time-slot", require("./TimeSlot"));

//exporting the router
module.exports = router;
