// import express
const express = require("express");
// calling router
const router = express.Router();
// importing timeSlot controller
const bookingController = require("../controllers/bookingController");

//to add a timeslot
router.post("/create", bookingController.create);

//exporting the router
module.exports = router;
