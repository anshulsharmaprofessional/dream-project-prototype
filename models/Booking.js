// importing mongoose instance
const mongoose = require("mongoose");

const bookingSchema = new mongoose.Schema(
  {
    timeSlot: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SingleTimeSlot",
    },
    // bookedSlotNumber: {
    //   type: Number,
    //   unique: true,
    // },
    name: {
      type: String,
      required: true,
    },
    birthplace: {
      type: String,
      required: true,
    },
    currentLocation: {
      type: String,
      required: true,
    },
    birthTime: {
      type: String,
      required: true,
    },
    birthDate: {
      type: Date,
      required: true,
    },
    contactNumber: {
      type: Number,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    isBooked: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Booking = mongoose.model("Booking", bookingSchema);

//exporting the schema to be used further
module.exports = Booking;
