// importing mongoose instance
const mongoose = require("mongoose");

const allTimeSlotSchema = new mongoose.Schema(
  {
    availableTimeSlots: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SingleTimeSlot",
      },
    ],
    bookedTimeSlots: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SingleTimeSlot",
      },
    ],
  },
  {
    timestamps: true,
  }
);

const AllTimeSlotSchema = mongoose.model(
  "allTimeSlotSchema",
  allTimeSlotSchema
);

//exporting the schema to be used further
module.exports = AllTimeSlotSchema;
