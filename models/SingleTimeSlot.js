// importing mongoose instance
const mongoose = require("mongoose");

const singleTimeSlotSchema = new mongoose.Schema(
  {
    slotNumber: {
      type: Number,
      unique: true,
    },
    timeDuration: {
      type: Number,
      required: true,
    },
    startTime: {
      type: String,
      required: true,
    },
    endTime: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    day: {
      type: Number,
      required: true,
    },
    month: {
      type: Number,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
    isBooked: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

const SingleTimeSlot = mongoose.model("singleTimeSlot", singleTimeSlotSchema);

//exporting the schema to be used further
module.exports = SingleTimeSlot;
