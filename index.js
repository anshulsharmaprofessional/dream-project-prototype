// import express
const express = require("express");

let cors = require("cors");

//Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
const bodyParser = require("body-parser");
//running express app
const app = express();

// const corsOption = {
//   origin: [process.env.PORT],
// };
app.use(cors());

require("dotenv").config();

// setting port number to 8080
const PORT = 8080;

// mongoose configuration
const db = require("./config/mongoose");

// using middleware for getting data from user
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());

//use express router
app.use("/", require("./routes"));

//This app starts a server and listens on port 8080 for connections.
//
app.listen(process.env.PORT || PORT, function (err) {
  if (err) {
    console.log(`Error in running the server: ${err}`); //not connected
  }
  console.log(`Server is running on port: ${PORT}`); //connected successfully
});
