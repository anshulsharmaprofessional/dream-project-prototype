const SingleTimeSlot = require("../models/SingleTimeSlot");
const AllTimeSlot = require("../models/AllTimeSlot");

module.exports.create = async function (req, res) {
  // catching server error
  try {
    // console.log("body :-", req.body);
    let slotNumber = 1;
    const last = await SingleTimeSlot.findOne()
      .sort({ field: "asc", _id: -1 })
      .limit(1);
    if (last) {
      slotNumber = last.slotNumber + 1;
    }
    const TimeSlot = await SingleTimeSlot.create({
      ...req.body,
      slotNumber: slotNumber,
      isBooked: 0,
    });
    // conformation api response
    if (TimeSlot) {
      // console.log("timeSlot", TimeSlot);
      const allTimeSlot = await AllTimeSlot.findOne();
      if (allTimeSlot && allTimeSlot.length != 0) {
        // console.log("this is if", TimeSlot, "all time slot \n", allTimeSlot);
        allTimeSlot.availableTimeSlots.push(TimeSlot);
      } else {
        // console.log("this is else", allTimeSlot);
        let timeslots = [];
        timeslots.push(TimeSlot);
        allTimeSlot = await AllTimeSlot.create({
          availableTimeSlots: timeslots,
        });
      }
      await allTimeSlot.save();
      return res.status(200).json({
        message: "timeSlot Added Successfully!",
        TimeSlot,
      });
    } else {
      // error Handling
      return res.status(421).json({
        message: "Error while adding Time Slot : Bad Request",
      });
    }
  } catch (err) {
    console.log("Error :" + err);
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};

module.exports.getTimeSlot = async function (req, res) {
  // catching server error
  try {
    const allTimeSlot = await AllTimeSlot.find().exec();
    // conformation api response
    if (allTimeSlot) {
      let allAvailableTimeSlotsId = allTimeSlot[0].availableTimeSlots;
      let allBookedTimeSlotsId = allTimeSlot[0].bookedTimeSlots;
      let allAvailableTimeSlots = [];
      let allBookedTimeSlots = [];
      // console.log("allTimeSlot", allTimeSlot);
      let slots = await SingleTimeSlot.find().exec();
      if (slots) {
        allAvailableTimeSlots = allAvailableTimeSlotsId.map((slotId) =>
          slots.find((slot) => slot.id == slotId)
        );
        allBookedTimeSlots = allBookedTimeSlotsId.map((slotId) =>
          slots.find((slot) => slot.id == slotId)
        );
        console.log("All Slots", allAvailableTimeSlots);
        return await res.status(200).json({
          message: "Here are all the time Slots schema",
          allAvailableTimeSlots,
          allBookedTimeSlots,
        });
      } else {
        return res.status(421).json({
          message: "Error while fetching all time slots schema",
        });
      }
    } else {
      return res.status(421).json({
        message: "Error while fetching time slots",
      });
    }
  } catch (err) {
    console.log("Error :" + err);
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
module.exports.getTimeSlotWithBooked = async function (req, res) {
  // catching server error
  try {
    const slots = await SingleTimeSlot.find().exec();
    // conformation api response
    if (slots) {
      return await res.status(200).json({
        message: "Here are all the time Slots",
        slots,
      });
    } else {
      return res.status(421).json({
        message: "Error while fetching time slots",
      });
    }
  } catch (err) {
    console.log("Error :" + err);
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
module.exports.getTimeSlotByDate = async function (req, res) {
  // catching server error
  try {
    const day = req.query.day;
    const month = req.query.month;
    const year = req.query.year;
    // console.log(dateString);

    // dateOject.setFullYear(year);
    // dateOject.setMonth(month);
    // dateOject.setDate(day);
    // "1995-12-17T03:24:00";
    const slot = await SingleTimeSlot.find({ day, month, year }).exec();

    if (slot) {
      return await res.status(200).json({
        message: "Date ->",
        slot,
      });
    } else {
      return res.status(421).json({
        message: "Error in Date",
      });
    }
  } catch (err) {
    console.log("Error :" + err);
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
