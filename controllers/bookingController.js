const Booking = require("../models/Booking");
const SingleTimeSlot = require("../models/SingleTimeSlot");

module.exports.create = async function (req, res) {
  // catching server error
  try {
    let slotNumber = req.query.timeSlotNumber;
    console.log("body :-", slotNumber);

    let slot = await SingleTimeSlot.findOne({ slotNumber: slotNumber });
    if (slot) {
      console.log("slot ID", slot);
      slot.isBooked = 1;
      slot.save();

      // allTimeSlot.availableTimeSlots =
      //   allTimeSlot.availableTimeSlots.filter((e) => e != slot.id);
      // console.log("all slotes", allTimeSlot.availableTimeSlots);
      // allTimeSlot.bookedTimeSlots.push(slot.id);
      // console.log("all booked slotes", allTimeSlot.bookedTimeSlots);

      // allTimeSlot.save();
      let booking = await Booking.create({
        ...req.body,
        timeSlot: slot.id,
      });
      // conformation api response
      if (booking) {
        return res.status(200).json({
          message: "successfully Booked",
          booking,
        });
      } else {
        // error Handling
        return res.status(421).json({
          message: "Error while Booking : Bad Request",
        });
      }
    } else {
      return res.status(421).json({
        message: "the slot does not exists",
      });
    }
  } catch (err) {
    console.log("Error :" + err);
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
